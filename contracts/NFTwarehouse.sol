//SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "hardhat/console.sol";
import { Base64 } from "./libraries/Base64.sol";

// Inherit the imported contract
contract NFTwarehouse is ERC721URIStorage {
  // Given by the OpenZeppelin contract to keep track of token IDs
  using Counters for Counters.Counter;
  Counters.Counter private _tokenIds;

  constructor() ERC721 ('SweetNFT', 'SWEET') {
    console.log('This is a SWEET NFT contract.');
  }

  string baseSvg = "<svg xmlns='http://www.w3.org/2000/svg' preserveAspectRatio='xMinYMin meet' viewBox='0 0 350 350'><style>.base { fill: white; font-family: serif; font-size: 24px; }</style><rect width='100%' height='100%' fill='black' /><text x='50%' y='50%' class='base' dominant-baseline='middle' text-anchor='middle'>";

  // I create three arrays, each with their own theme of random words.
  // Pick some random funny words, names of anime characters, foods you like, whatever!
  string[] firstWords = ["Sauron", "Aragorn", "Gandalf", "Frodo", "Legolas", "Gollum", "Arwen", "Bilbo", "Gimli", "Boromir", "Pippin", "Galadriel", "Samwise", "Saruman", "Merry", "Elrond", "Haldir", "Theoden", "Isildur"];
  string[] secondWords = ["Admiral_Ackbar", "Mace_Windu", "Greedo", "Jawa", "C3PO", "R2D2", "Qui-Gon_Jinn", "Padme_Amidala", "Poe_Dameron", "Luke_Skywalker", "Leia_Organa", "Darth_Vader", "Emperor_Palpatine", "Anakin_Skywalker", "Lando_Calrissian"];
  string[] thirdWords = ["Karate", "Kung_Fu", "Judo", "Muay_Thai", "Brazilian_Jiu-Jitsu", "Krav_Maga", "Aikido", "Boxing", "Wrestling", "MCMAP", "Fencing", "Duel", "Sumo", "Kendo", "Aikido"];

  // I create a function to randomly pick a word from each array.
  function pickRandomFirstWord(uint256 tokenId) public view returns (string memory) {
    // I seed the random generator. More on this in the lesson.
    uint256 rand = random(string(abi.encodePacked("FIRST_WORD", Strings.toString(tokenId))));
    // Squash the # between 0 and the length of the array to avoid going out of bounds.
    rand = rand % firstWords.length;
    return firstWords[rand];
  }

  function pickRandomSecondWord(uint256 tokenId) public view returns (string memory) {
    uint256 rand = random(string(abi.encodePacked("SECOND_WORD", Strings.toString(tokenId))));
    rand = rand % secondWords.length;
    return secondWords[rand];
  }

  function pickRandomThirdWord(uint256 tokenId) public view returns (string memory) {
    uint256 rand = random(string(abi.encodePacked("THIRD_WORD", Strings.toString(tokenId))));
    rand = rand % thirdWords.length;
    return thirdWords[rand];
  }

  function random(string memory input) internal pure returns (uint256) {
      return uint256(keccak256(abi.encodePacked(input)));
  }

  function makeNFT() public {
    // Get the current tokenId, starts at 0
    uint256 tokenId = _tokenIds.current();

    // We go and randomly grab one word from each of the three arrays.
    string memory first = pickRandomFirstWord(tokenId);
    string memory second = pickRandomSecondWord(tokenId);
    string memory third = pickRandomThirdWord(tokenId);
    string memory phrase = string(abi.encodePacked(first, second, third));

    // I concatenate it all together, and then close the <text> and <svg> tags.
    string memory finalSvg = string(abi.encodePacked(baseSvg, phrase, "</text></svg>"));
    console.log("\n--------------------");
    console.log(finalSvg);
    console.log("--------------------\n");

     // Get all the JSON metadata in place and base64 encode it.
    string memory json = Base64.encode(
        bytes(
            string(
                abi.encodePacked(
                    '{"name": "',
                    // We set the title of our NFT as the generated word.
                    phrase,
                    '", "description": "A highly acclaimed collection of squares.", "image": "data:image/svg+xml;base64,',
                    // We add data:image/svg+xml;base64 and then append our base64 encode our svg.
                    Base64.encode(bytes(finalSvg)),
                    '"}'
                )
            )
        )
    );

     // Just like before, we prepend data:application/json;base64, to our data.
    string memory finalTokenUri = string(
        abi.encodePacked("data:application/json;base64,", json)
    );

    console.log("\n--------------------");
    console.log(finalTokenUri);
    console.log("--------------------\n");

    // Mint the NFT to the sender using msg.sender
    _safeMint(msg.sender, tokenId);

    // Set the NFT's data
    _setTokenURI(tokenId, finalTokenUri);


    console.log("An NFT w/ ID %s has been minted to %s", tokenId, msg.sender);

    // Increment our counter
    _tokenIds.increment();
  }
}